#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int randmultiplo(amp,p){
  int v=rand()%amp;
  while ((v%p)!=0){
    v=rand()%amp;
  }
  return v;
}


void preencheVetor(int vet[],int tam, int amp, int p){
  int i;
  for(i=0;i<tam;i++){
    vet[i]=randmultiplo(amp,p);
  }
}



void MostraP(int vet[],int tam,int p){
  int i;
  printf("Posicoes de p:\n");
  for(i=0;i<tam;i++){
      if (vet[i]==p)
        printf("%d ",i);
  }
}

void mostraVetor(int vet[],int tam){
  int i;
  printf("Exibe Valores:\n");
  for(i=0;i<tam;i++){
    printf("%d:%d\n",i,vet[i]);
  }
}


int contaElemento(int vet[],int tam, int elemento){
  int i;
  int count=0;
  for(i=0;i<tam;i++){
      if (vet[i]==elemento) count++;
  }
  return count;
}

void contarTodosElementos(int vet[], int tam){
  int i;
  printf("\nQuantidade de ocorrências de cada número\n");
  for (i=0;i<tam;i++){
    printf("p%d:%d -> %d ocorrencia[s]\n",i,vet[i],contaElemento(vet,tam,vet[i]));
  }
}


int contaPares(int vet[],int tam){
  int i,count=0;
  for(i=0;i<tam;i++){
    if ((vet[i]%2)==0){
      count++;
    }
  }
  return count;
}

void copiaVetorParidade(int vet[],int tamvet,int cop[],int tamcop, int resto){
  int i,j=0;
  for (i=0;i<tamvet;i++){
    if ((vet[i]%2)==resto){
      cop[j]=vet[i];
      j++;
    }
  }
}



int main (void){
  srand( (unsigned)time(NULL) );
  int tam,amp,p;
  printf("Quantidade de posições do vetor\n");
  scanf("%d",&tam);
  int vet[tam];
  printf("Amplitude do vetor\n");
  scanf("%d",&amp);
  printf("Informe o multiplo\n");
  scanf("%d",&p);
  preencheVetor(vet,tam,amp,p);
  mostraVetor(vet,tam);
  MostraP(vet,tam,p);
  contarTodosElementos(vet,tam);
  int pares=contaPares(vet,tam);
  int impares=tam-pares;
  int Vp[pares];
  int Vi[impares];
  printf("\nElementos pares %d \n Elementos Impares %d \n",pares,impares);
  copiaVetorParidade(vet,tam,Vp,pares,0);
  copiaVetorParidade(vet,tam,Vi,impares,1);
  printf("\nMostra vetor par:\n");
  mostraVetor(Vp,pares);
  printf("\nMostra vetor impar\n");
  mostraVetor(Vi,impares);

  getchar();
  return 0;
}
